﻿using UnityEngine;
using System.Collections;

public class CharacterStatus : MonoBehaviour {


	public CameraLockedController controller;

	//Baseline Statistics
	public int _BaseMight;
	public int _BaseSleight;
	public int _BaseForesight;
	public int _BaseBlight;

	public int _Health, _MaxHealth;
	public float _BaseStamina, _MaxStamina;

	public float _StamCooldown;
	public int _StamDrainMod;

	//Modified Stats
	public float _ActStam;

	public void AdjustStamina()
	{
		if (controller.sprinting) {
			
		}
	}

	void Awake() {

		_Health = 100;
		_MaxHealth = 100;
		_BaseStamina = 100.0f;
		_MaxStamina = 100.0f;

		_BaseMight = 10;
		_BaseSleight = 10;
		_BaseForesight = 10;
		_BaseBlight = 0;

	}


	// Use this for initialization
	void Start () {
	
	//	controller = GameObject.Find ("Player").GetComponent<CameraLockedController> ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
