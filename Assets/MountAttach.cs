﻿using UnityEngine;
using System.Collections;

public class MountAttach : MonoBehaviour {

	public Transform parent;

	// Use this for initialization
	void Start () {
		transform.SetParent (parent);
		MountAttach.Destroy (this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
