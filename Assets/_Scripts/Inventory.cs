﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	public int slotsX, slotsY;
	public List<Item> inventory = new List<Item>();
	public List<Item> equipment = new List<Item>();
	public List<Item> eSlots = new List<Item> ();
	public List<Item> slots = new List<Item> ();
	private ItemDatabase database;
	private bool showInventory;
	public GUISkin skin;
	private bool showTooltip;
	private string tooltip;
	GameObject Player;

	private bool draggingItem;
	private Item draggedItem;
	private int prevIndex;

	// Use this for initialization
	void Start () {
		showInventory = false;
		for (int i = 0; i < (slotsX * slotsY); i++) {
			slots.Add (new Item ());
			inventory.Add (new Item ());
		}

		for (int i = 0; i < 6; i++) {
			eSlots.Add (new Item ());
			equipment.Add (new Item());
		}

		eSlots [0].subType = Item.EquipType.Weapon;
		eSlots [1].subType = Item.EquipType.Head;
		eSlots [2].subType = Item.EquipType.Neck;
		eSlots [3].subType = Item.EquipType.Torso;
		eSlots [4].subType = Item.EquipType.Legs;
		eSlots [5].subType = Item.EquipType.Trinket;

		database = GameObject.Find ("ItemDatabase").GetComponent<ItemDatabase> ();
		//
		AddItem (0);
		AddItem (1);
		AddItem (2);

		print (InventoryContains (1, inventory));
		print (InventoryContains (0, equipment));
	}

	void Update()
	{
		if (Player == null) {
			Player = GameObject.Find("Player");
		}
		if (Input.GetKeyDown (KeyCode.T)) {
			showInventory = !showInventory;
		}
	}

	// Update is called once per frame
	void OnGUI () {
	
		tooltip = "";
		GUI.skin = skin;
		if (showInventory) {
			DrawInventory ();
			DrawEquipment();
		} else {
			showTooltip = false;
		}
		if(showTooltip)
		{
			GUI.Box (new Rect(Event.current.mousePosition.x + 15f, Event.current.mousePosition.y, 200, 200), tooltip, skin.GetStyle("Tooltip"));
		}
		if(draggingItem)
		{
			GUI.DrawTexture(new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, 50, 50), draggedItem.itemIcon);
		}
	}

	void DrawInventory()
	{

		float guiRatio = Screen.width / 1920.0f;

		int i = 0;
	
		for(int y = 0; y < slotsY; y++)
		{
			//draws y range
			for(int x = 0; x < slotsX; x++)
			{
				//draws x range
				Rect slotRect = new Rect (((x * 70) * guiRatio) + 100 * guiRatio, ((y * 70) * guiRatio) + 100 * guiRatio, guiRatio * 64, guiRatio * 64);
				GUI.Box (slotRect, "", skin.GetStyle("slot"));
				slots[i] = inventory[i];
				//the slots share the inventory data

				//if the slot isn't empty
				if(slots[i].itemName != null)
				{
					//draw the texture
					GUI.DrawTexture(slotRect, slots[i].itemIcon);
					//If the rectangle area contains a mouse
					if(slotRect.Contains(Event.current.mousePosition))
					{
						//show the tooltip
						tooltip = CreateTooltip(slots[i]);
						showTooltip = true;

						//If you click and drag and aren't currently holding an item
						if(Event.current.button == 0 && Event.current.type == EventType.MouseDrag && !draggingItem)
						{
							//pick up the item and clear slot
							draggingItem = true;
							prevIndex = i;
							draggedItem = slots[i];
							inventory[i] = new Item();
						}
						//if you let go of the mouse and you're dragging an item
						if(Event.current.type == EventType.MouseUp && draggingItem)
						{
							//drop item in slot, and swap item with previous slot
							inventory[prevIndex] = inventory[i];
							inventory[i] = draggedItem;
							draggingItem = false;
							draggedItem = null;
						}
					}

				} else {
					//if the slot IS empty
					if(slotRect.Contains(Event.current.mousePosition))
					{
						//if you let go and are dragging an item
						if(Event.current.type == EventType.MouseUp && draggingItem )
						{
							//drop item in empty slot
							inventory[i] = draggedItem;
							draggingItem = false;
							draggedItem = null;
						}
					} 
				}

				if (tooltip == "")
				{
					showTooltip = false;
				}
				i++;

			}
		}

		//DRAW EQUIPMENT BELOW

	}

	void DrawEquipment()
	{
		float guiRatio = Screen.width / 1920.0f;


		int j = 0;
		for (int z = 0; z < 6; z++) {
			Rect EquipRect = new Rect ((1 * 70 * guiRatio) + 250 * guiRatio, (z * 70 * guiRatio) + 150 * guiRatio, 64 * guiRatio, 64 * guiRatio);
			GUI.Box(EquipRect, "", skin.GetStyle("slot"));
			eSlots[j] = equipment[j];
			eSlots [0].subType = Item.EquipType.Weapon;
			eSlots [1].subType = Item.EquipType.Head;
			eSlots [2].subType = Item.EquipType.Neck;
			eSlots [3].subType = Item.EquipType.Torso;
			eSlots [4].subType = Item.EquipType.Legs;
			eSlots [5].subType = Item.EquipType.Trinket;
			if(eSlots[j].itemName != null)
			{
				GUI.DrawTexture(EquipRect, eSlots[j].itemIcon);
				
				if(EquipRect.Contains(Event.current.mousePosition))
				{
					tooltip = CreateTooltip(eSlots[j]);
					showTooltip = true;
					
					if(Event.current.button == 0 && Event.current.type == EventType.MouseDrag && !draggingItem)
					{
						//pick up the item and clear slot
						draggingItem = true;
						prevIndex = j;
						draggedItem = eSlots[j];
						Player.GetComponentInChildren<AnimHandler> ().ItemDequip (equipment [j]);
						print(draggedItem.subType);
						equipment[j] = new Item();
						if (equipment [j].subType == Item.EquipType.Weapon) {
							Player.GetComponentInChildren<Animator> ().runtimeAnimatorController = Player.GetComponentInChildren<AnimHandler> ().noWeapon;
							GameObject.Destroy (Player.GetComponent<CameraLockedController> ().Weapon);
						}

					}
				}
			} else { 
				
				if(EquipRect.Contains(Event.current.mousePosition))
				{
					if(Event.current.type == EventType.MouseUp && draggingItem && draggedItem.subType == eSlots[j].subType)
					{
						print(eSlots[j].subType);
						print(draggedItem.subType);
						print(j);
						//drop item in empty slot
						equipment[j] = draggedItem;
						draggingItem = false;
						draggedItem = null;
						if (equipment [j].wType == Item.WeaponType.Broadsword) {
							Player.GetComponentInChildren<Animator> ().runtimeAnimatorController = Player.GetComponentInChildren<AnimHandler> ().Broadsword;
						}
						Player.GetComponentInChildren<AnimHandler>().ItemEquip(equipment[j]);
					}
				}
			}
			j++;
		}
	}



	string CreateTooltip(Item item)
	{
		tooltip = item.itemName + "\n <color=#FFFF00> <size=16> <i> " + item.itemDesc + " </i> </size> </color>";
		return tooltip;
	}

	void AddItem(int id)
	{
		for (int i = 0; i < inventory.Count; i++)
		{
			if (inventory[i].itemName == null)
			{
				for(int j = 0; j < database.items.Count; j++)
				{
					if(database.items[j].itemID == id)
					{
						inventory[i] = database.items[j];
					}
				}
				break;
			}
		}
	}

	bool InventoryContains(int id, List<Item> it)
	{
		bool result = false;
		for (int i = 0; i < it.Count; i++) 
		{
			result = it[i].itemID == id;
			if (result)
			{
				break;
			}
		}
		return result;
	}

	void ItemEquipped(int id)
	{

	}
}
