﻿using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {

	// Use this for initialization
	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.GetComponent<CameraLockedController> () != null) {
			collision.collider.GetComponent<CameraLockedController>().Hitpoints = 0;
		}
	}
}
