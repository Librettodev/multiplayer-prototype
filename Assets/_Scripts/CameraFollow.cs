﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public GameObject Player;
	float temp;
	float tempX;
	float tempZ;
	[Range (2,10)] public float currentY;
	public float followDistance;
	bool turnRight;
	bool turnLeft;
	public float CamTurnSpd;
	Vector3 direction;
	Vector3 idealPos;

	void Start()
	{
	}

	void Update()
	{
		idealPos = Player.transform.TransformPoint (0,5,-10);


		if (Input.GetKey (KeyCode.RightArrow) ) {
			turnRight = true;
		}

		if (Input.GetKeyUp (KeyCode.RightArrow) ) {
			turnRight = false;
		}

		if (Input.GetKey (KeyCode.LeftArrow) ) {
			turnLeft = true;
		} 
		if (Input.GetKeyUp (KeyCode.LeftArrow) ) {
			turnLeft = false;
		}
		direction = transform.position - Player.transform.position;
		direction.Normalize ();
		//smoothly rotate towards player

		//print (idealPos.localPosition + " or " + idealPos.position );
	}

	void FixedUpdate()
	{		
		transform.position = Vector3.Slerp (transform.position, idealPos, CamTurnSpd);
		temp = Vector3.Distance (transform.position, Player.transform.position);
		if (temp > followDistance) {
			transform.position = Vector3.MoveTowards (transform.position, Player.transform.position, 1f * Time.deltaTime * (temp - followDistance));
		} else if (temp < (followDistance / 2)) {
			transform.Translate(direction * Time.deltaTime, Space.World);
		}
		tempX = transform.position.x;
		tempZ = transform.position.z;
		transform.position = new Vector3(tempX,currentY,tempZ);
		//for physical things
		if (turnRight) {
			transform.RotateAround(Player.transform.position, Vector3.up, -CamTurnSpd * temp * Time.deltaTime);
		}
		if (turnLeft) {
				transform.RotateAround(Player.transform.position, Vector3.up, CamTurnSpd * temp * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			transform.Translate(Vector3.up * Time.deltaTime, Space.World);
			currentY += 1 * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.Translate(Vector3.down * Time.deltaTime, Space.World);
			currentY -= 1 * Time.deltaTime;
		}

		currentY = Mathf.Clamp (currentY, 2, 10);
	}
}
