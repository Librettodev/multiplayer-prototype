﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraLockedController : MonoBehaviour {

	public float airSpeed;
	public float turnSpeed;
	public float iFrames;
	float tempiFrames;
	public float Hitpoints;
	Rigidbody rb;
	bool jumping;
	float distToGround;
	[HideInInspector]
	public bool sprinting;
	public bool Grounded;
	public bool forwardMomentum;
	public bool backwardMomentum;
	public float airControl;
	public Animator anim;
	public GameObject Rigger;
	[HideInInspector]
	public GameObject Weapon;

	//Debug code below
	Vector3 camForward;
	Vector3 groundNormal;
	public Transform mCam;
	public Vector3 netMove;
	[HideInInspector]
	public Quaternion lastFaced;
	Vector3 netMoveForward;
	Vector3 targetDir;

	public bool LockOn;
	public Transform LockTarget;
	bool invincible;

	// Use this for initialization
	void Awake () {
		rb = transform.GetComponent<Rigidbody> ();
		LockOn = false;
		Hitpoints = 100.0f;
	}
	
	void Start() {
		distToGround = transform.GetComponent<Collider> ().bounds.extents.y;
		print (distToGround);
		if (Camera.main == null) {
            SpawnCam();
        } 
		lastFaced = transform.rotation;
		LockTarget = GameObject.Find ("Target").transform;
	}

	IEnumerator BeginTimeout()
	{
		invincible = true;
		yield return new WaitForSeconds(tempiFrames);
		invincible = false;
		yield return null;
	}
    // Update is called once per frame
    void Update() {
        Grounded = isGrounded();
        if (anim == null) {
            anim = transform.GetComponentInChildren<Animator>();
        }
        targetDir = LockTarget.position - transform.position;
        targetDir.Normalize();
        transform.InverseTransformDirection(targetDir);
        if (Input.GetKeyDown(KeyCode.Space)) {
            jumping = true;
            Grounded = false;
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
            sprinting = true;
        }
        if (Input.GetKeyDown(KeyCode.E)) {
            anim.SetBool("LightAttack", true);
        }
        if (Input.GetKeyDown(KeyCode.Space)) {
            anim.SetBool("Stun", true);
        }
        if (Input.GetKeyDown(KeyCode.F) && anim.GetCurrentAnimatorStateInfo(0).IsName("FreeMove")) {
            anim.SetBool("HeavyAttack", true);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl)) {
            LockOn = !LockOn;
            anim.SetBool("LockOn", LockOn);
        }

		if (Hitpoints <= 0) {
			anim.SetBool("Dead", true);
			anim.ResetTrigger("Stagger");
			anim.ResetTrigger ("Knockback");
		}
		GameObject.Find ("Canvas").GetComponentInChildren<Slider> ().value = Hitpoints / 100.0f;
	}

	bool isGrounded()
	{
		Debug.DrawLine(transform.position + new Vector3(0,0.1f,0), transform.position - new Vector3(0,0.2f,0), Color.blue);
		return Physics.Raycast (transform.position + new Vector3(0,0.1f,0), -Vector3.up, 0.3f);

	}

	[PunRPC]
	public void Destruction()
	{
		if (PhotonNetwork.isMasterClient) {
			GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player.ID);
			PhotonNetwork.Destroy(gameObject);
		}
	}

	[PunRPC]
	public void DamageReceive(int damage, int intensity, Vector3 pos)
	{
		if (!invincible) {
			Hitpoints -= damage;
			print ("Damage Received was " + damage);
			if (intensity == 1) {
				tempiFrames = iFrames;
				StartCoroutine("BeginTimeout");
				anim.SetTrigger ("Stagger");
			} else if (intensity == 2) {
				tempiFrames = iFrames * 6.2f;
				StartCoroutine("BeginTimeout");
				anim.SetTrigger ("Knockback");
			}
			transform.rotation = Quaternion.LookRotation(Vector3.Scale((pos - transform.position).normalized, new Vector3(1, 0, 1)).normalized);
			lastFaced = transform.rotation;

		}
	}

	void MoveCalc()
	{
		float h = 0;
		float v = 0;
		if (Input.GetKey (KeyCode.D)) {
			h += 1;
		} 
		
		if (Input.GetKey (KeyCode.A)) {
			h += -1;
		}

		if (Input.GetKey (KeyCode.W)) {
			v += 1;
		}

		if (Input.GetKey (KeyCode.S)) {
			v += -1;
		} 

		if (LockOn == false) {
			camForward = Vector3.Scale (mCam.forward, new Vector3 (1, 0, 1)).normalized;
			netMove = (v * camForward) + (h * mCam.right);
			print (netMove);
			Debug.DrawRay(transform.position, camForward, Color.red);
			Debug.DrawRay(transform.position, mCam.right, Color.blue);
			Debug.DrawRay(transform.position, netMove.normalized, Color.green);

		} else {
			camForward = Vector3.Scale (targetDir, new Vector3 (1, 0, 1)).normalized;
			netMove = v * targetDir + h * transform.right;

		}
	}

	void ApplyMove()
	{
		Vector3 alteredNetMove = transform.InverseTransformDirection (netMove);
		GroundStats ();
		if (LockOn) {
			anim.SetFloat("Parallel", alteredNetMove.x, 0.1f, Time.deltaTime);
		}
		alteredNetMove = Vector3.ProjectOnPlane(alteredNetMove, groundNormal);
		if (!sprinting) {
			alteredNetMove = alteredNetMove / 2;
		}
		if (isGrounded ()) {
			anim.SetFloat ("Forward", alteredNetMove.z, 0.05f, Time.deltaTime);
			GetComponent<CapsuleCollider> ().material.dynamicFriction = 1;
		} else {
			anim.SetFloat ("Forward", 0, 1.0f, Time.deltaTime);
			GetComponent<CapsuleCollider> ().material.dynamicFriction = 0;
		}
	}

	void ApplyRot()
	{ 
//Below is the code for LockOn vs Non-LockOn rotation handling.
		if (LockOn == false && anim.GetCurrentAnimatorStateInfo (0).IsName ("FreeMove")) {
			if (netMove != Vector3.zero && !sprinting) {
				transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (netMove.normalized), Time.deltaTime * turnSpeed);
				lastFaced = transform.rotation;
			} else if (netMove != Vector3.zero && sprinting) {
				transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (netMove.normalized), Time.deltaTime * (turnSpeed / 2));
				lastFaced = transform.rotation;
			} else {
				transform.rotation = lastFaced;
			}
			//for Lock-On handling vvvv
		} else if (LockOn == false) {
			transform.rotation = lastFaced;
		} else {
			transform.rotation = Quaternion.LookRotation (Vector3.Scale (targetDir, new Vector3 (1, 0, 1)).normalized);
		}
	}
	public void QuickRot()
	{
		if (Vector3.Magnitude(netMove.normalized) > 0.1f) {
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (netMove.normalized), Time.deltaTime * turnSpeed);
			lastFaced = transform.rotation;
		}
	}

	void GroundStats()
	{
		RaycastHit hitI;
		if (Physics.Raycast (transform.position + new Vector3(0,0.1f,0), Vector3.down, out hitI, 0.1f)) {
		groundNormal = hitI.normal;
		}
	}

	void SpawnCam()
	{
		if(mCam == null)
		{
			GameObject mC;
			mC = Instantiate(Rigger, new Vector3(0.0f, 1.5f, 0.0f), Quaternion.identity) as GameObject;
			mC.GetComponent<CameraFollowOrbit>().Player = transform;
			mCam = mC.GetComponentInChildren<Camera>().transform;
            mC.name = "Rigger";
		}
	}

	void FixedUpdate()
	{
		if (mCam != null) {
			MoveCalc ();
			ApplyMove ();
			ApplyRot ();

			sprinting = false;
		} else {
			if(GameObject.Find("Rigger(Clone)") != null && (mCam == null))
			{
				mCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
			} else {
				SpawnCam ();
			}
		}
	}
}
