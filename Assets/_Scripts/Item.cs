﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item {

	public string itemName;
	public int itemID;
	public string itemDesc;
	public Texture2D itemIcon;
	public ItemType itemType;
	public int sellCost;
	public EquipType subType;
	public WeaponType wType;
	public GameObject worldObj;

	public int damage;

	public enum WeaponType {
		Broadsword,
		Dualblades,
		Crossbow
	}

	public enum ItemType {
		Equipment,
		Consumable,
		Basic
	}

	public enum EquipType {
		Null,
		Weapon,
		Head,
		Neck,
		Torso,
		Legs,
		Trinket,
	}

	public Item(string name, int id, string desc, ItemType type, int cost)
	{
		itemName = name;
		itemID = id;
		itemDesc = desc;
		itemType = type;
		sellCost = cost; 
		itemIcon = Resources.Load <Texture2D> ("ItemIcons/" + name);
	}

	public Item(string name, int id, string desc, ItemType type, int cost, EquipType eType)
	{
		itemName = name;
		itemID = id;
		itemDesc = desc;
		itemType = type;
		sellCost = cost; 
		if (Resources.Load <Texture2D> ("ItemIcons/" + name) != null) {
			itemIcon = Resources.Load <Texture2D> ("ItemIcons/" + name);
		} else {
			itemIcon = Resources.Load <Texture2D> ("ItemIcons/Key");
		}
		if (itemType == ItemType.Equipment) {
			subType = eType;
		} else {
			Debug.LogError ("NOT A REAL EQUIPMENT TYPE!");
		}
	}

	public Item(string name, int id, string desc, ItemType type, int cost, EquipType eType, int dmg, WeaponType weap)
	{
		itemName = name;
		itemID = id;
		itemDesc = desc;
		itemType = type;
		sellCost = cost; 
		itemIcon = Resources.Load <Texture2D> ("ItemIcons/" + name);
		
		if (itemType == ItemType.Equipment) {
			subType = eType;
		} else {
			Debug.LogError ("NOT A REAL EQUIPMENT TYPE!");
		}

		wType = weap;
		damage = dmg;

		
	}

	public Item()
	{
		itemID = -1;
	}
}
