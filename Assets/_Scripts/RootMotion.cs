﻿using UnityEngine;
using System.Collections;

public class RootMotion : MonoBehaviour {

	public CameraLockedController controller;
	float netMoveForward;

	void Start()
	{
		controller = transform.parent.GetComponent<CameraLockedController> ();
	}
	
	void OnAnimatorMove() {
		Animator animator = GetComponent<Animator>();
		Transform pTransform = transform.parent;
		pTransform.position += animator.deltaPosition;
	}
}
