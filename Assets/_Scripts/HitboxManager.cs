﻿using UnityEngine;
using System.Collections;

public class HitboxManager : MonoBehaviour {

	public bool isLive;
	[HideInInspector]
	public int KOFactor;
	[HideInInspector]
	public int Damage;
	public int DamageMod;
	int TorsoID;
	int BellyID;
	GameObject tempNet;
	GameObject tempSelf;
	// Use this for initialization
	void Awake () {
		isLive = false;
	}
	void Start()
	{
		TorsoID = GenFun.FindParentWithName (this.gameObject, "Torso 1").transform.GetInstanceID ();
		BellyID = GenFun.FindParentWithName (this.gameObject, "Belly").transform.GetInstanceID ();
	}
	void Update()
	{
		if (isLive && (GetComponent<BoxCollider> ().enabled == false) && (GenFun.FindParentWithName(this.gameObject, "Player") != null)) {
			GetComponent<BoxCollider> ().enabled = true;
		} else if (!isLive) {
			GetComponent<BoxCollider>().enabled = false;
		}
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider other)
	{
		if (isLive && (other.name != "Player")) {
		//Fire events
			print("You hit " + other.name + "!");
			print("Damage was " + Damage + ", and Intensity was " + KOFactor);
			//At some point, this code below needs to be optimized. Try assigning tags during instantiation.
			//it IS a network thing, after all.
			//Might cause issues with NPC attacks.
			if((other.name == "Torso" || other.name == "Belly" ) && ((other.transform.GetInstanceID() != TorsoID) && (other.transform.GetInstanceID() != BellyID)))
			{
				print(GenFun.FindParentWithName(this.gameObject, "Belly"));
				print(other.gameObject);
				if(GenFun.FindParentWithName(other.gameObject, "Player(Clone)").name == "Player(Clone)"){
				tempNet = GenFun.FindParentWithName(other.gameObject, "Player(Clone)");
				}
				GameObject tempself = GenFun.FindParentWithName(this.gameObject, "Player");
				print(GenFun.FindParentWithName(other.gameObject, "Player(Clone)"));
				if(tempNet.name == "Player(Clone)")
				{
				tempNet.GetComponent<CameraLockedController>().GetComponent<PhotonView>().RPC("DamageReceive", PhotonTargets.All, Damage, KOFactor, tempself.transform.position);
				tempNet.transform.rotation = Quaternion.LookRotation(Vector3.Scale((tempself.transform.position - tempNet.transform.position).normalized, new Vector3(1, 0, 1)).normalized);
				tempNet.GetComponent<CameraLockedController>().lastFaced = tempNet.transform.rotation;
				} else {
					tempself.GetComponent<CameraLockedController>().GetComponent<PhotonView>().RPC("DamageReceive", PhotonTargets.All, Damage, KOFactor);
					tempself.transform.rotation = Quaternion.LookRotation(Vector3.Scale((tempNet.transform.position - tempself.transform.position).normalized, new Vector3(1, 0, 1)).normalized);
					tempself.GetComponent<CameraLockedController>().lastFaced = tempself.transform.rotation;
				}
			}
		}
	}
}
