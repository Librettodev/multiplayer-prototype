﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimHandler : MonoBehaviour {


	/*For Now, AnimHandler holds all the computing logic for rendering the character, including armorpieces and weapons. This may change
	at a future date.*/

	public Transform WeaponPoint;
	public Transform EquipPoint;

	GameObject tempPrefab;
	GameObject rWeapon;
	Collider WeaponHitbox;
	Animator anim;
	public RuntimeAnimatorController noWeapon;
	public RuntimeAnimatorController Broadsword;

	public List<SkinnedMeshRenderer> Meshes = new List<SkinnedMeshRenderer>();
	//public List<Item> IRef = new List<Item> ();
	public GameObject gHead, gTorso, gLegs;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		Meshes.Add (transform.Find ("Head").GetComponent<SkinnedMeshRenderer>());
		Meshes.Add (transform.Find ("Hair").GetComponent<SkinnedMeshRenderer>());
		Meshes.Add (transform.Find ("Torso").GetComponent<SkinnedMeshRenderer>());
		Meshes.Add (transform.Find ("Arms").GetComponent<SkinnedMeshRenderer>());
		Meshes.Add (transform.Find ("Legs").GetComponent<SkinnedMeshRenderer>());
	}
	
	// Update is called once per frame
	void Update () {
		if (rWeapon != null) {
			if (WeaponHitbox.GetComponent<HitboxManager> ().isLive) {
				if (anim.GetCurrentAnimatorStateInfo (0).IsName ("Slash") || anim.GetCurrentAnimatorStateInfo (0).IsName ("SecondSlash")) {
					WeaponHitbox.GetComponent<HitboxManager> ().Damage = 15;
					WeaponHitbox.GetComponent<HitboxManager> ().KOFactor = 1;
				}
				if (anim.GetCurrentAnimatorStateInfo (0).IsName ("Heavy")) {
					WeaponHitbox.GetComponent<HitboxManager> ().Damage = 45;
					WeaponHitbox.GetComponent<HitboxManager> ().KOFactor = 2;
				}
				if (anim.GetCurrentAnimatorStateInfo (0).IsName ("SprintAttack")) {
					WeaponHitbox.GetComponent<HitboxManager> ().Damage = 25;
					WeaponHitbox.GetComponent<HitboxManager> ().KOFactor = 2;
				}
			}
		}
		if (tempPrefab != null) {
			tempPrefab = null;
		}
	}

	public void HitboxStart()
	{
		//check to see if the weapon even exists. If it does, it probably has a hitbox.
		//Otherwise wacky stuff'll happen.
		//THEREFORE, ALL WEAPONS MUST HAVE A HITBOX.
		if(rWeapon != null) 
		{
			WeaponHitbox.GetComponent<HitboxManager> ().isLive = true;
			if(GenFun.FindParentWithName(this.gameObject, "Player") != null){
				GenFun.FindParentWithName(this.gameObject, "Player").GetComponent<CameraLockedController>().QuickRot(); 
			} else {
				GenFun.FindParentWithName(this.gameObject, "Player(Clone)").GetComponent<CameraLockedController>().QuickRot();
			}
		} 
	}

	// This needs to be reworked; it was hardcoded for testing.
	//TODO: fix WeaponPoint problem; 
	public void ItemEquip(Item a)
	{
		if (a.subType == Item.EquipType.Weapon) {
			rWeapon = GameObject.Instantiate (a.worldObj, WeaponPoint.transform.position, Quaternion.identity) as GameObject;
			rWeapon.transform.SetParent (WeaponPoint, true);
			rWeapon.transform.localRotation = Quaternion.Euler (Vector3.zero);
			rWeapon.GetComponent<HitboxManager> ().enabled = true;
			WeaponHitbox = rWeapon.GetComponent<BoxCollider> ();
			GenFun.FindParentWithName (this.gameObject, "Player").GetComponent<CameraLockedController> ().Weapon = rWeapon;
			print (WeaponHitbox);
		} else if (a.subType != Item.EquipType.Neck && a.subType != Item.EquipType.Trinket) {

			tempPrefab = GameObject.Instantiate (a.worldObj, transform.parent.transform.position, Quaternion.identity) as GameObject;
			tempPrefab.transform.SetParent (transform.parent.transform, true);
			tempPrefab.transform.localRotation = Quaternion.Euler (Vector3.zero);
			tempPrefab.GetComponent<BoneSync> ().enabled = true;
			//IRef.Add (a);
		}

		//Masking for equipmen
		switch(a.subType)
		{
		case Item.EquipType.Head:
			gHead = tempPrefab;
			Meshes [0].enabled = false;
			Meshes [1].enabled = false;
			break;
		case Item.EquipType.Torso:
			gTorso = tempPrefab;
			Meshes[2].enabled = false;
			break;
		case Item.EquipType.Legs:
			gLegs = tempPrefab;
			Meshes[4].enabled = false;
			break;
		default:
			break;
		}
	}

	public void ItemDequip(Item a)
	{
		switch (a.subType) {
		case Item.EquipType.Head:
			GameObject.Destroy (gHead);
			Meshes [0].enabled = true;
			Meshes [1].enabled = true;
			break;
		case Item.EquipType.Torso:
			GameObject.Destroy (gTorso);
			Meshes[2].enabled = true;
			break;
		case Item.EquipType.Legs:
			GameObject.Destroy (gLegs);
			Meshes[4].enabled = true;
			break;
		default:
			break;
		}
	}

	public void HitboxEnd()
	{
		if (rWeapon != null) {
			WeaponHitbox.GetComponent<HitboxManager> ().isLive = false;
			WeaponHitbox.GetComponent<HitboxManager> ().Damage = 0;
			WeaponHitbox.GetComponent<HitboxManager> ().KOFactor = 0;
		}
	}

	public void Die()
	{
		if (GenFun.FindParentWithName (gameObject, "Player") != null) {
			GenFun.FindParentWithName (gameObject, "Player").GetComponent<PhotonView> ().RPC ("Destruction", PhotonTargets.All);
		}
	}
	public void SlashReset()
	{
		GetComponent<Animator> ().SetBool ("LightAttack", false);
	}

	public void StunReset()
	{
		GetComponent<Animator> ().SetBool ("Stun", false);
	}

	public void HeavyReset()
	{
		GetComponent<Animator> ().SetBool ("HeavyAttack", false);
	}
}
