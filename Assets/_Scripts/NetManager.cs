﻿using UnityEngine;
using System.Collections;

public class NetManager : MonoBehaviour {
	//TODO IN FUTURE: Convert this to handling movement from splash lobby to game.
	public GameObject chatclient;
	public GameObject LoadingCam;
	bool waiting;

	public static NetManager instance = null;
	// Use this for initialization

	void Awake()
	{
		waiting = false;
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}
	void Start () 
	{
		PhotonNetwork.sendRate = 20;
		PhotonNetwork.sendRateOnSerialize = 20;
		Connect ();
		LoadingCam = GameObject.Find ("LoadingCam");
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.P) && (GameObject.Find ("Player") == null)) {
			SpawnMyPlayer();
		}
		if (PhotonNetwork.masterClient == null && Input.GetKeyDown (KeyCode.O) && PhotonNetwork.connectedAndReady) {

			PhotonNetwork.JoinRandomRoom();
		}

		if (Input.GetKeyDown (KeyCode.I) && PhotonNetwork.masterClient != null) {
			PhotonNetwork.LeaveRoom();
		}

	}

	void Connect()
	{
		PhotonNetwork.ConnectUsingSettings ("HuntedFew v02");
	}

	void OnGUI() 
	{
		GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());
	}

	void OnConnectedToMaster()
	{
		Debug.Log ("OnJoinedLobby");
		//PhotonNetwork.JoinRandomRoom ();
		waiting = false;

	}


	void OnPhotonRandomJoinFailed() 
	{
		Debug.Log ("OnPhotonRandomJoinFailed");
		PhotonNetwork.CreateRoom (PhotonNetwork.player.userId);
	}

	void OnJoinedRoom()
	{
		Debug.Log ("OnJoinedRoom");
		LoadingCam.SetActive (false);
		SpawnMyPlayer ();
	
	}

	void OnLeftRoom()
	{
		Debug.Log ("OnLeftRoom");
		LoadingCam.SetActive (true);
		Destroy (GameObject.Find ("Rigger(Clone)"));
		waiting = true;
	}

	void SpawnMyPlayer() {
		GameObject go;
		go = PhotonNetwork.Instantiate ("Player", new Vector3 (0.0f, 1.5f, 0.0f), Quaternion.identity, 0);
		go.name = "Player";
		go.GetComponent<CameraLockedController> ().enabled = true;
		go.GetComponentInChildren<RootMotion>().enabled = true;
	}

}
