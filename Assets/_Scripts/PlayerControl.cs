﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public float jumpForce;
	public float leapForce;
	public float moveSpeed;
	public float turnSpeed;
	Rigidbody rb;
	bool jumping;
	float distToGround;
	bool sprinting;
	public bool Grounded;
	public bool forwardMomentum;
	public bool backwardMomentum;
	public float airControl;
	public Animator anim;
	Transform animMesh;
	// Use this for initialization
	void Awake () {
		rb = transform.GetComponent<Rigidbody> ();
		animMesh = GameObject.Find ("lowpolypolished").GetComponent<Transform> ();

	}

	void Start() {
		distToGround = transform.GetComponent<Collider> ().bounds.extents.y;
		print (distToGround);

	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			jumping = true;
			Grounded = false;
		}
		Grounded = isGrounded ();
		if (Input.GetKey (KeyCode.LeftShift)) {
			sprinting = true;
		}
		transform.position = (animMesh.position + new Vector3(0f,1.5f,0f));
		animMesh.localPosition = Vector3.Slerp(animMesh.localPosition, new Vector3(0f,-1.5f,0f), 1.0f);
	}

	bool isGrounded()
	{
		return Physics.Raycast (transform.position, -Vector3.up, distToGround + 0.01f);
	}

	void FixedUpdate()
	{

		if (Input.GetKey (KeyCode.D)) {
			transform.Rotate(0,1 * turnSpeed,0, Space.World);
		}

		if (Input.GetKey (KeyCode.A)) {
			transform.Rotate(0,-1 * turnSpeed,0, Space.World);
		}
		//The below handlers deal with 1, if the player is moving forward on the ground
		//2, the player was running and then jumped
		//3, the player is in the air and trying to move
		//4, the player is stopped on the ground.

		if (Input.GetKey (KeyCode.W) && Grounded) {
			if(sprinting)
			{
				transform.Translate (0, 0, 1 * Time.deltaTime * moveSpeed * 2);
				forwardMomentum = true;
			} else {
				forwardMomentum = true;
				anim.SetInteger("AnimState", 3);
			}
		} else if (forwardMomentum && !Grounded) {
			if(sprinting) {
				rb.AddForce (transform.forward * leapForce * 3, ForceMode.Impulse); 
				forwardMomentum = false;
			} else {
				rb.AddForce (transform.forward * leapForce, ForceMode.Impulse); 
				forwardMomentum = false;
			}
		} else if (Input.GetKey (KeyCode.W) && !Grounded) {
			rb.AddForce(transform.forward * airControl);
			forwardMomentum = false;
		} else {
			anim.SetInteger ("AnimState", 0);
			forwardMomentum = false;
		}

		if (Input.GetKey (KeyCode.S) && Grounded) {
			transform.Translate(0,0,-1 * Time.deltaTime * moveSpeed / 2);
			backwardMomentum = true;
		} else if (backwardMomentum && !Grounded) {
			rb.AddForce (-transform.forward * jumpForce, ForceMode.Impulse); 
			backwardMomentum = false;
		} else if (Input.GetKey (KeyCode.S) && !Grounded) {
			rb.AddForce(-transform.forward * airControl / 2);
			backwardMomentum = false;
		} else {
			backwardMomentum = false;
		}
		//Applies jump force
	if (jumping) {
			if(sprinting)
			{
				rb.AddForce(0.0f, jumpForce / 2, 0.0f, ForceMode.Impulse);	
				jumping = false;
			} else {
				rb.AddForce(0.0f, jumpForce, 0.0f, ForceMode.Impulse);	
				jumping = false;
			}
		}
	if (Grounded) {
			rb.velocity = Vector3.zero;
		}
		sprinting = false;
	}
}
