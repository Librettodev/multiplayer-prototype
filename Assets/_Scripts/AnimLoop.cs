﻿using UnityEngine;
using System.Collections;

public class AnimLoop : MonoBehaviour {
	public int animState;
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		anim.SetInteger ("AnimState", animState);
	}
}
