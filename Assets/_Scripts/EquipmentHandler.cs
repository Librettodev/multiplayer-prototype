﻿using UnityEngine;
using System.Collections;

public class EquipmentHandler : MonoBehaviour {

	public GameObject Weapon;
	public GameObject BackStrap;
	public GameObject WeaponEquipPoint;
	// Use this for initialization
	void Start () {
		Weapon = transform.GetComponentInChildren<WeaponHandler>().gameObject;
		BackStrap = transform.Find ("Armature/Root/Belly/Torso/BackStrap").gameObject;
		WeaponEquipPoint = transform.Find ("Armature/Root/Belly/Torso/Shoulder.R/UpperArm.R/ForeArm.R/Hand.R/EquipPoint").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void EquipPos (string type) {
		if(type == "Weapon"){
			if(Weapon.transform.parent.name == "BackStrap")
			{
				Weapon.transform.SetParent(WeaponEquipPoint.transform, false);
			} 
		}
		if (type == "Sheath") {
			if(Weapon.transform.parent.name == "BackStrap")
			{
				Weapon.transform.SetParent(WeaponEquipPoint.transform, false);
			} else if (Weapon.transform.parent.name == "EquipPoint")
			{
				Weapon.transform.SetParent(BackStrap.transform, false);
			}
		}
	}
}
