﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour {

	public List<Item> items = new List<Item>();

	//Holds all data for items, which then define models, textures, etc.
	//Pulls item definition from Item.cs

	void Start()
	{
		items.Add (new Item ("Key", 0, "A rather mundane looking key", Item.ItemType.Basic, 5));
		items.Add (new Item ("Crude Blade", 1, "A ramshackle sword that has seen a fair bit of use.", Item.ItemType.Equipment, 10, Item.EquipType.Weapon)); 
		items.Add (new Item ("Rough Leather Cuirass", 2, "A shoddy and stained leather tunic.", Item.ItemType.Equipment, 15, Item.EquipType.Torso));
		items [1].worldObj = Resources.Load <GameObject> ("Crude Sword");
		items [2].worldObj = Resources.Load <GameObject> ("_Prefabs/Props/Armor/Rough Leather Cuirass");
	}
}