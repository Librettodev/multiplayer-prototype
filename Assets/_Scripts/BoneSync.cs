﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoneSync : MonoBehaviour {

	public enum ArmorType
	{
		Head,
		Torso,
		Legs,
		TorsoArms
	};

	public ArmorType id;
	public Transform cArmature;
	public List<Transform> ArmorBones;
	public List<Transform> CharacterBones;
	// Use this for initialization

	// TO DO: Write BoneSync function, which will be used in Instantiation, as START() isn't a great way to do it
	// Also, write out full bonesyncs for fingers (UGGGH)
	void Start () {
		//myBone = transform.Find("Armature/Belly");
		//myBone2 = transform.Find ("Armature/Belly/Torso");

		if (cArmature == null) {
			if (GameObject.Find ("Player/segmentmodel") != null) {
				cArmature = GameObject.Find ("Player/segmentmodel").transform;
			}
		}

		print (cArmature.name);

		if (id == ArmorType.Torso ) {
			ArmorBones.Add (transform.Find ("Armature/Belly"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.L"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.R"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.L/UpperArm.L"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.R/UpperArm.R"));

			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.L"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.R"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.L/UpperArm.L"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.R/UpperArm.R"));
		}

		if (id == ArmorType.Legs) {
			ArmorBones.Add (transform.Find("Armature/Belly"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.L"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.R"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.L/Shin.L"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.R/Shin.R"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.L/Shin.L/Heel.L"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.R/Shin.R/Heel.R"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.L/Shin.L/Heel.L/Toes.L"));
			ArmorBones.Add (transform.Find("Armature/Belly/Thigh.R/Shin.R/Heel.R/Toes.R"));

			CharacterBones.Add (transform.Find("Armature/Root/Belly"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.L"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.R"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.L/Shin.L"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.R/Shin.R"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.L/Shin.L/Heel.L"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.R/Shin.R/Heel.R"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.L/Shin.L/Heel.L/Toes.L"));
			CharacterBones.Add (transform.Find("Armature/Root/Belly/Thigh.R/Shin.R/Heel.R/Toes.R"));
		}

		if (id == ArmorType.Head) {
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Neck"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Neck/Head"));

			CharacterBones.Add (transform.Find ("Armature/Root/Belly/Torso 1/Neck"));
			CharacterBones.Add (transform.Find ("Armature/Root/Belly/Torso 1/Neck/Head"));

		}

		if (id == ArmorType.TorsoArms) {
			ArmorBones.Add (transform.Find ("Armature/Belly"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.L"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.R"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.L/UpperArm.L"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.R/UpperArm.R"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.L/UpperArm.L/ForeArm.L"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.R/UpperArm.R/ForeArm.R"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.L/UpperArm.L/ForeArm.L/Hand.L"));
			ArmorBones.Add (transform.Find ("Armature/Belly/Torso/Shoulder.R/UpperArm.R/ForeArm.R/Hand.R"));

			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.L"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.R"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.L/UpperArm.L"));
			CharacterBones.Add (cArmature.Find ("Armature/Root/Belly/Torso 1/Shoulder.R/UpperArm.R"));
			CharacterBones.Add (transform.Find ("Armature/Root/Belly/Torso 1/Shoulder.L/UpperArm.L/ForeArm.L"));
			CharacterBones.Add (transform.Find ("Armature/Root/Belly/Torso 1/Shoulder.R/UpperArm.R/ForeArm.R"));
			CharacterBones.Add (transform.Find ("Armature/Root/Belly/Torso 1/Shoulder.L/UpperArm.L/ForeArm.L/Hand.L"));
			CharacterBones.Add (transform.Find ("Armature/Root/Belly/Torso 1/Shoulder.R/UpperArm.R/ForeArm.R/Hand.R"));
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (cArmature == null) {
			if (GameObject.Find ("Player/segmentmodel") != null) {
				cArmature = GameObject.Find ("Player/segmentmodel").transform;
			}
			}

		for (int i = 0; i < ArmorBones.Count; i++) {
		
			ArmorBones[i].position = CharacterBones[i].position;
			ArmorBones[i].rotation = CharacterBones[i].rotation;

		}
	}
}
