﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextManager : MonoBehaviour {


	Text text;
	// Use this for initialization
	void Start () {
		text = GameObject.Find ("DeadMessage").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<Slider> ().value == 0) {
			text.enabled = true;
		} else {
			text.enabled = false;
		}
	}
}
