﻿using UnityEngine;
using System.Collections;

public class CameraFollowOrbit : MonoBehaviour {

	public Transform Player;
	//handles how quickly camera slerps to new position
	public float AdjustFactor;
	Vector3 IdealPos;
	Vector3 TargetDir;
	float TurnAmount;
	Vector3 CamDir;
	float Diff;
	
	void FixedUpdate () {
		if (Player != null) {
			if (Player.GetComponent<CameraLockedController> () != null && Player.GetComponent<CameraLockedController>().LockOn) {
				TargetDir = Player.GetComponent<CameraLockedController> ().LockTarget.position - transform.position;
				TargetDir.Normalize ();
				CamDir = transform.GetChild (0).position - transform.position;
				transform.InverseTransformDirection (TargetDir);
				CamDir.Normalize ();
				Diff = (Mathf.Atan2 (-TargetDir.x, -TargetDir.z) - Mathf.Atan2 (CamDir.x, CamDir.z));
			}
			if (Player.GetComponent<CameraLockedController> ().LockOn) {
				if (Vector2.Angle (new Vector2 (CamDir.x, CamDir.z), new Vector2 (TargetDir.x, TargetDir.z)) < 169.0f) {
					transform.GetChild (0).GetComponent<OrbiterScript> ().LockRotate (Diff, Vector2.Angle (new Vector2 (CamDir.x, CamDir.z), new Vector2 (TargetDir.x, TargetDir.z)));
				}
			}
			transform.position = Vector3.Slerp (transform.position, Player.position, AdjustFactor * Time.fixedDeltaTime);
		} else {
			if(GameObject.Find ("Player") != null)
			{
			Player = GameObject.Find("Player").transform.Find("Belly").transform;
			Debug.Log ("The rig camera didn't attach properly!");
			}
		}
	}
}
