﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon.Chat;
using ExitGames.Client.Photon;

public class ChatHandler : MonoBehaviour, IChatClientListener {


	ChatHandler m_Instance;
	ChatClient m_ChatClient;
	bool m_SubscribedToRoom;
	string ChatUsername;
	string m_RoomChannelName;
	bool m_Connected;
	Queue<string> m_Messages;

	public string chatAppId;
	public string RoomChannelName;

	void Awake()
	{
		//Only one Chathandler should exist
		if (m_Instance != null && m_Instance != this) {

			Destroy(gameObject);
			return;
		}

		m_ChatClient = new ChatClient (this);

		m_Instance = this;

		DontDestroyOnLoad (gameObject);

		Application.runInBackground = true;

	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (m_ChatClient == null) {
			return;
		}

		if (m_SubscribedToRoom == false /*&& Player.LocalPlayer != null*/) {
		
			JoinRoomChannels();
		}
		m_ChatClient.Service ();

	}

	public void Connect()
	{
		m_ChatClient.Connect (chatAppId, "1.0", null);
	}

	void JoinRoomChannels()
	{
		m_SubscribedToRoom = true;

		m_RoomChannelName = "room" + PhotonNetwork.room.name.GetHashCode();

		m_ChatClient.Subscribe (new string[] {m_RoomChannelName});
	}

	void LeaveRoomChannels()
	{
		if (m_RoomChannelName != null)
		{
			m_ChatClient.Unsubscribe( new string[] {m_RoomChannelName});
		}

		m_SubscribedToRoom = false;
	}

	public bool IsConnected()
	{
		return m_Connected;
	}

	public void SendText( string text, string channel, bool isPrivateMessage)
	{
		if (text == "") {
			return;
		}

		if (isPrivateMessage == true) {
			m_ChatClient.SendPrivateMessage (channel, text);
		} else {

			m_ChatClient.PublishMessage(channel, text);
		}
	}

	public Queue<string> GetMessages()
	{
		return m_Messages;
	}

	public string GetReadableChannelName( string channel )
	{
		if (channel == RoomChannelName) {
			return "Room";
		}

		return "Global";
	}

	public void OnApplicationQuit()
	{
		if (m_ChatClient == null) {
			return;
		}

		if (m_Connected == true && m_SubscribedToRoom == false) {
		
		}

		m_ChatClient.Disconnect ();
	}

	public void OnDisconnected()
	{
		Debug.Log( "Disconnected: " + m_ChatClient.DisconnectedCause);
	}

	public void OnConnected()
	{
		m_Connected = true;

	}

	public void DebugReturn(DebugLevel uhOhLevel, string ohNo)
	{
		Debug.Log("!!!!" + " " + ohNo);
	}

	public void OnChatStateChange(ChatState state)
	{

	}

	public void OnGetMessages(string CchannelName, string[] Ssenders, object[] Mmessages)
	{

	}

	public void OnPrivateMessage(string Ssender, object Mmessage, string CchannelName)
	{

	}

	public void OnSubscribed(string[] channel, bool[] result)
	{

	}

	public void OnUnsubscribed(string[] channel)
	{

	}

	public void OnStatusUpdate(string uuser, int sstatus, bool ggotMessage, object mmessage)
	{

	}
}
