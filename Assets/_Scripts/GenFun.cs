﻿using UnityEngine;
using System.Collections;

public class GenFun : MonoBehaviour {

	public static GameObject FindParentWithName(GameObject childObject, string name)
	{
		Transform t = childObject.transform;
		while (t.parent != null)
		{
			if(t.parent.name == name)
			{
				print("gameobject was " + t.parent.gameObject);
				return t.parent.gameObject;
			}
			t = t.parent.transform;
		}
		print ("Could not find Parent. Check your spelling perhaps?");
		return null;
	}
}
