﻿using UnityEngine;
using System.Collections;

public class MyNetworkCharacter : Photon.MonoBehaviour {


	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;
	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponentInChildren<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (photonView.isMine) {
			//Do nothing; this is already handled.
		} else {
			transform.position = Vector3.Lerp(transform.position, realPosition, 0.1f);
			transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 0.1f);
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		
		if (stream.isWriting) {
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			//stream.SendNext (GetComponent<CameraLockedController>().lightNetBool);
			//stream.SendNext (anim.GetBool("HeavyAttack"));
			//stream.SendNext (anim.GetBool("Stun"));
			//OUR player. Send pos to network.

		} else if (stream.isReading) {
			//This is someone else's player, receive their position and update our version of their player.
			realPosition = (Vector3)stream.ReceiveNext();
			realRotation = (Quaternion)stream.ReceiveNext();
			//print(stream.PeekNext());
			//anim.SetBool ("LightAttack", (bool)stream.ReceiveNext());
			//anim.SetBool ("HeavyAttack", (bool)stream.ReceiveNext());
			//anim.SetBool ("Stun", (bool)stream.ReceiveNext());

		}
	}
}
