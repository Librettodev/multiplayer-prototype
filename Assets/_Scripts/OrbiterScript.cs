﻿using UnityEngine;
using System.Collections;

public class OrbiterScript : MonoBehaviour {

	public Transform RigCenter;
	public float orbitSpeed;
	public Transform LookTar;
	Transform Player;
	float comp;
	float angComp;

	//right/true, false/left
	bool comInput;

	Vector3 idealPos;
	Quaternion idealRot;

	int layerMask;

	void Start () {
		Player = transform.parent.GetComponent<CameraFollowOrbit> ().Player;
        RigCenter = transform.parent;
        LookTar = GameObject.Find("Belly").transform;
	
		idealPos = transform.localPosition;
		idealRot = transform.rotation;
		layerMask = 1 << 8;
		layerMask = ~layerMask;

	}

	public void LockRotate(float DiffNum, float angMag)
	{
	
			angMag = 170 - angMag;
		if (angMag < 170.1f) {
		//	angMag = Mathf.Lerp (angComp, angMag, Time.deltaTime);
			if (DiffNum < -3.5f || DiffNum < 3.5f && Mathf.Sign (DiffNum) == 1) {
				transform.RotateAround (RigCenter.position, Vector3.up, angMag / orbitSpeed);
				comInput = true;
				comp = 1.0f;
			} else if (DiffNum > 3.5f || DiffNum > -3.5f && Mathf.Sign (DiffNum) == -1) {
				transform.RotateAround (RigCenter.position, Vector3.up, -angMag / orbitSpeed);
				comp = 1.0f;
				comInput = false;
			}
			angComp = angMag;
		}

	}

	void FixedUpdate () {
		if (Player != null) {
			if (Player.GetComponent<CameraLockedController> ().LockOn /*&& Target == null*/) {
				//Target = Player.GetComponent<CameraLockedController> ().LockTarget;
			}
			if (Input.GetKey (KeyCode.LeftArrow)) {
				RigCenter.transform.Rotate (Vector3.up, -orbitSpeed);
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				RigCenter.transform.Rotate (Vector3.up, orbitSpeed);
			}
			if (Input.GetKey (KeyCode.UpArrow)) {
				transform.RotateAround (RigCenter.position, RigCenter.transform.right, orbitSpeed);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				transform.RotateAround (RigCenter.position, RigCenter.transform.right, -orbitSpeed);
			}
		} else if (transform.parent.GetComponent<CameraFollowOrbit>().Player != null){
			Player = transform.parent.GetComponent<CameraFollowOrbit>().Player;
		}

		if (DistToTarget () < 5.8f && (Physics.Raycast (transform.position, -transform.forward, 0.5f, layerMask) != true)) {
			transform.position -= transform.forward * Time.deltaTime * 2;
		} else if (DistToTarget () > 7.0f) {
			transform.localPosition = idealPos;
			transform.rotation = idealRot;
			transform.LookAt(LookTar);
		}
	}

	void CameraClip()
	{
		RaycastHit hitI;
		if (Physics.Raycast(LookTar.position, transform.position - LookTar.position, out hitI, 5.8f, layerMask)) {
			if(hitI.collider.name != "Player" && hitI.collider.name != "Belly" && hitI.collider.name != "Torso")
				{
				transform.position = hitI.point;
				}
			}
	}

	float DistToTarget()
	{
		return Vector3.Distance (transform.position, RigCenter.transform.position);
	}

	void LateUpdate()
	{
		CameraClip ();
		if (transform.eulerAngles.x > 60.0f && transform.eulerAngles.x < 80.0f) {
			transform.RotateAround (RigCenter.position, RigCenter.transform.right, -(transform.eulerAngles.x - 60.0f));
		} else if (transform.eulerAngles.x < 300.0f && transform.eulerAngles.x > 280.0f) {
			transform.RotateAround (RigCenter.position, RigCenter.transform.right, -(transform.eulerAngles.x - 300.0f));
		}
	}
}